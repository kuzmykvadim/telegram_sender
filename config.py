from dotenv  import load_dotenv
import os
from logger.custom_logger import configure_logger
load_dotenv()


# logger init 2 logs (DEBUG/ERROR) files, and send error to monitoring channel
log_sender = configure_logger('log_sender', os.getenv('SENDER_DEBUG_LOGGER_PATH'), os.getenv('SENDER_ERROR_LOGGER_PATH'))

# content directory
CONTENT_PATH = os.getenv("CONTENT_PATH")

# telegram_send module use .conf files for send content
SENDER_CONFIG_PATH = os.getenv('SENDER_CONFIG_PATH')

# specific config for every channel - see content_config.json.example
CONTENT_CONFIG_JSON = os.getenv('CONTENT_CONFIG_JSON')

# sent content save (chat_id,sent_content_name) in .csv file
SENT_CONTENT = os.getenv('SENT_CONTENT')