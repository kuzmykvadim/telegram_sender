# TeleApp

script for sending content to telegram channels

### Instalation

install virtual and activate environment 

    > python -m venv venv
    > source venv/bin/activate

install requirements

    > pip install -r requirements.txt

clone and install logger package

    > git clone https://bitbucket.org/kuzmykvadim/logger/src/master/
    > python logger/setup.py install

copy and edit .env file with right paraameters
    
    > cp env.example .env

change channel config file `channel_id.conf` with of your channels in `sender_configs` directory


write configs of channel in .content_config.json file

    > cp content_config.json.example content_config.json
    


### Usage

    > python sender.py --chat_id you_chat_id