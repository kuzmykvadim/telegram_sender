import telegram_send
import argparse
import json
import os.path
import glob
import random
from config import (
    CONTENT_PATH,
    CONTENT_CONFIG_JSON,
    SENT_CONTENT,
    SENDER_CONFIG_PATH
)


class ArgParser:
    def __init__(self):
        parser = argparse.ArgumentParser(usage='./%(prog)s command --file FILE')
        parser.add_argument('--chat_id',
                    nargs='?',
                    help='set telegram chat_id\n')

        self.args = parser.parse_args()


class Sender():
    """
        This class accepts the channel, randomly takes the content for the channel 
        according to the configuration, and sends it to the specified telegram channel
    """


    def __init__(self):
        self.arg = ArgParser()
        self.chat_id = ArgParser().args.chat_id


    def get_channel_config(self):
        """
            get chat config object by chat_id 
        """
        chat_id = self.arg.args.chat_id
        channel_conf = None
        with open(CONTENT_CONFIG_JSON, "rb") as conf_file:
            conf = json.load(conf_file)
            for item in conf:
                if item['chat_id'] == chat_id:
                    channel_conf = item
        if channel_conf:
            return(channel_conf)
        else:
            log_sender.critical(f'Channel {chat_id} config not found')
            exit()


    @staticmethod
    def check_if_content_sent(file, chat_id):
        """
            This method to check if this content has been posted before.
        """
        
        to_check = f'{chat_id},{file}'
        
        mode = 'r+' if os.path.exists(SENT_CONTENT) else 'w+'

        with open(SENT_CONTENT, mode) as sent_f:
            if mode == 'w+':
                sent_f.write('chat_id,sent_content\n')
            line_found = any(str(to_check) in line for line in sent_f)
            if not line_found:
                sent_f.seek(0, os.SEEK_END)
                if len(to_check) > 0:
                    sent_f.write(f"{to_check}\n")
                    return file
            else:
                log_sender.info(f'File {file} already sent to {chat_id}')
                return None


    
    def get_content(self):
        """
            This method randomly takes content from the specified content directory, 
            checks if it has been sent before, and returns the path to the content
        """
        chanel_conf = self.get_channel_config()
        insta_sources = chanel_conf.get('insta_sources')[0]
        category = chanel_conf.get('category')
        chat_id = chanel_conf.get('chat_id')
        content_desc = None

        # TODO add multiple sources

        content_path = os.path.join(CONTENT_PATH, category, insta_sources)
        json_path = os.path.join(CONTENT_PATH, category, insta_sources, insta_sources + ".json")
        
        os.chdir(content_path)        
        content_files = glob.glob("*.jpeg")

        # Get random content file
        res_file = None
        i = 0
        while res_file is None and i < 10:
            #if random has chosen content that has already been sent before, it choose another one
            i += 1
            try:
                file = random.choice(content_files)
                res_file = Sender.check_if_content_sent(file, chat_id)
            except:
                pass
        
        if res_file:
            f_path = f"{content_path}/{res_file}"
            return f_path
        else:
            log_sender.critical(f"Content not found, probably it empty for {chat_id}")
            exit()


    def send_to_channel(self):
        """
         This method get config file for specific channel 
         and send coosen content it to telegram channel
        """
        content = self.get_content()
        channel_conf = self.get_channel_config()
        sender_conf_file = SENDER_CONFIG_PATH + "/" + self.chat_id + ".conf"
        chat_id = channel_conf.get("chat_id")
        try:
            with open(content, "rb") as f:
                telegram_send.send(
                    images=[f],
                    conf=sender_conf_file,
                    captions=[f'@{chat_id}']
                    )
                log_sender.info(f'Post with {content} to {chat_id} sent successfully')

        except:
            log_sender.critical(f'Error Content {content} to channel {chat_id} not sent!')
            

if __name__ == '__main__':
    sender = Sender()
    sender.send_to_channel()


